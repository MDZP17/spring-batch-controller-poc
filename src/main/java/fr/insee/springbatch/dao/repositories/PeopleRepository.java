package fr.insee.springbatch.dao.repositories;

import fr.insee.springbatch.model.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PeopleRepository extends JpaRepository<People,Long> {
    List<People> findAll();

    People findByPersonId(long personId);
}
