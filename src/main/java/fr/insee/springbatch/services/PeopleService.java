package fr.insee.springbatch.services;

import fr.insee.springbatch.model.entity.People;

import java.util.List;
public interface PeopleService {
    /*
     * @return List<People> peoples
     */
    List<People> getAllPeople();
    /**
     * @return People people
     */
    People getPeopleById(long personId);
}
