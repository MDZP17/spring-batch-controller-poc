package fr.insee.springbatch.model.entity;
import javax.persistence.*;

@Entity
public class People {
    @Id
    @GeneratedValue(generator = "id_pers", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "id_pers", allocationSize = 1, sequenceName = "id_pers")
    private long personId;

    private String firstName;

    private String lastName;
    public People(){};
    public People(long personId,String firstName,String lastName) {
        this.personId=personId;
        this.firstName=firstName;
        this.lastName=lastName;
    }

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString(){
        return firstName + " " + lastName;
    }
}
